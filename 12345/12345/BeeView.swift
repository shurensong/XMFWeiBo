//
//  BeeView.swift
//  12345
//
//  Created by 邹花平 on 16/4/13.
//  Copyright © 2016年 邹花平. All rights reserved.
//

import UIKit

//1.定义代理
protocol  DearHoneyBeeDelegate : NSObjectProtocol
{
    //2.定义代理方法
    func  DearHoneyBeeGoToMakeHoney()
    
}

class BeeView: UIView
{
    //3.定义代理属性
    weak var delegate : DearHoneyBeeDelegate?
    
    //4.调用代理方法
    func   beeKingOrder()
    {
        delegate?.DearHoneyBeeGoToMakeHoney()
        
    }
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
