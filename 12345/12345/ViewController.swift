//
//  ViewController.swift
//  12345
//
//  Created by 邹花平 on 16/4/13.
//  Copyright © 2016年 邹花平. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?)
    {

           let honeyBeeView:BeeView = BeeView()
  
            view = honeyBeeView
            
            //5.设置代理
            
            honeyBeeView.delegate = self
            
            DearHoneyBeeGoToMakeHoney()
    }
    
}

//扩展
extension  ViewController : DearHoneyBeeDelegate
{
    //6.代理方法的实现
    func DearHoneyBeeGoToMakeHoney()
    {
        print("飞呀飞呀，飞到花丛中！！！")
    }
}

