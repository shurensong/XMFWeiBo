//
//  HMConst.swift
//  WeiBoPractise
//
//  Created by 邹花平 on 16/4/13.
//  Copyright © 2016年 邹花平. All rights reserved.
//

import UIKit
//Swift 只要不是在类里面的代码，全局都可以使用
///// 默认参数 file: String = __FILE__,如果不传入参数会使用 = 后面的作为参数
func HMPrint(items: Any , file: String = __FILE__ , line: Int = __LINE__ , function: String = __FUNCTION__)
{
    #if DEBUG
        
        print((file as NSString).lastPathComponent + " ,[\(line)]行，方法名称：" + function + " 输出：\(items)")
    
    #endif

}

//tabBar 的数量
let HMMainBarButtonCount = 5