//
//  HMBaseTableViewController.swift
//  WeiBoPractise
//
//  Created by 邹花平 on 16/4/10.
//  Copyright © 2016年 邹花平. All rights reserved.
//

import UIKit


class HMBaseTableViewController: UITableViewController
{
    
    //用户是否登录
    var userLogin = false
    
        override func loadView()
    {
        //没有登录就调用自定义的一个方法setupVistorView
        userLogin ? super.loadView() : setupVistorView()//三目运算符
    }
    


    //设置访客视图
    private func setupVistorView()
    {
        view = visitorView//就是我下面的HMVisitorView
        
        //5.设置访客视图的代理 ，当设置代理后，没有实现这个协议就会报错
        visitorView.delegate = self
        
        //设置访客视图内容 , 4个子控制器都会走这里, self具体是什么类型的看创建的子类: 多态 , 就是谁调用了这个方法就是谁 , 再则就是
        if self is HMHomeTableViewController
        {
            visitorView.startRotation()
        }
        else if self is HMMessageTableViewController
        {
            visitorView.setVisitorViewInfo("visitordiscover_image_message", message: "登录后，别人评论你的微博，发给你的消息，都会在这里收到通知")
            
        }
            
        else if self is HMDiscoverTableViewController
        {
            visitorView.setVisitorViewInfo("visitordiscover_image_message", message: "登录后，最新、最热微博尽在掌握，不再会与实事潮流擦肩而过")
            
        }
        else if self is HMProfileTableViewController
        {
            visitorView.setVisitorViewInfo("visitordiscover_image_profile", message: "登录后，你的微博、相册、个人资料会显示在这里，展示给别人")
            
        }
        
        
        
        //设置左右两个导航栏按钮, 注册和登录
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "注册", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("visitorViewDidClickRegisterButton"))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "登录", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("visitorViewDidClickLoginButton"))
        
        
    }
    
    //MARK: - 懒加载数据
    /// 给别人调用的时候尽量简单
    /// 高内聚低耦合
     lazy var visitorView: HMVisitorView = HMVisitorView()
    //相当于oc里面的[[HMVisitorView alloc] init]

    
}



//扩展和 oc 的分类很像，只能扩展方法，可以让对象实现协议，好处是方便管理代码
extension HMBaseTableViewController : HMVisitorViewDelegate
{
     //MARK: - 代理方法的实现
    func visitorViewDidClickLoginButton()
    {
        print("visitorViewDidClickLoginButton")
    }
    
    func visitorViewDidClickRegisterButton() {
        print("visitorViewDidClickRegisterButton")
    }

}
