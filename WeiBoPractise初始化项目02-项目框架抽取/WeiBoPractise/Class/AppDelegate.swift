//
//  AppDelegate.swift
//  WeiBoPractise
//
//  Created by 邹花平 on 16/4/9.
//  Copyright © 2016年 邹花平. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool
    {
        //创建Window
        window = UIWindow(frame:UIScreen.mainScreen().bounds)
        
//        let vc = UITabBarController()
//        
//        //设置根控制器
//        window?.rootViewController = vc
//        
//        vc.view.backgroundColor = UIColor.greenColor()
        
        
        let mainVc = HMMainViewController()
         //设置根控制器
        window?.rootViewController = mainVc
        //设为主窗口
        window?.makeKeyAndVisible()

        
//        //首页
//
//        let homeVc = HMHomeTableViewController()
//        
//        homeVc.title = "首页"
//        homeVc.tabBarItem.image = UIImage(named: "tabbar_home")
//        
//        vc.addChildViewController(UINavigationController(rootViewController: homeVc))
//        
//        //消息
//        let messageVc = HMMessageTableViewController()
//        messageVc.title = "消息"
//        messageVc.tabBarItem.image = UIImage(named: "tabbar_message_center")
//        vc.addChildViewController(UINavigationController(rootViewController: messageVc))
//        
//        //发现
//        let discoverVc = HMDiscoverTableViewController()
//        discoverVc.title = "发现"
//        discoverVc.tabBarItem.image = UIImage(named: "tabbar_discover")
//        vc.addChildViewController(UINavigationController(rootViewController: discoverVc))
//        
//        //我
//        let profifleVc = HMProfileTableViewController()
//        profifleVc.title = "我"
//        profifleVc.tabBarItem.image = UIImage(named: "tabbar_profile")
//        vc.addChildViewController(UINavigationController(rootViewController: profifleVc))
        
        
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

