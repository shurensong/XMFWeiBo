//
//  HMMainViewController.swift
//  WeiBoPractise
//
//  Created by 邹花平 on 16/4/10.
//  Copyright © 2016年 邹花平. All rights reserved.
//

import UIKit

class HMMainViewController: UITabBarController {

    override func viewDidLoad()
    {
        super.viewDidLoad()

        //首页
        
        //        let homeVc = HMHomeTableViewController()
        //        homeVc.title = "首页"
        //        homeVc.tabBarItem.image = UIImage(named: "tabbar_home")
        //        addChildViewController(UINavigationController(rootViewController: homeVc))
        setupChildViewController(HMHomeTableViewController(), imageName: "tabbar_home", title: "首页")
        
        //消息
        //        let messageVc = HMMessageTableViewController()
        //        messageVc.title = "消息"
        //        messageVc.tabBarItem.image = UIImage(named: "tabbar_message_center")
        //        addChildViewController(UINavigationController(rootViewController: messageVc))
        setupChildViewController(HMMessageTableViewController(), imageName: "tabbar_message_center", title: "消息")
        
        //发现
        //        let discoverVc = HMDiscoverTableViewController()
        //        discoverVc.title = "发现"
        //        discoverVc.tabBarItem.image = UIImage(named: "tabbar_discover")
        //        addChildViewController(UINavigationController(rootViewController: discoverVc))
        setupChildViewController(HMDiscoverTableViewController(), imageName: "tabbar_discover", title: "发现")
        
        //我
        //        let profifleVc = HMProfileTableViewController()
        //        profifleVc.title = "我"
        //        profifleVc.tabBarItem.image = UIImage(named: "tabbar_profile")
        //        addChildViewController(UINavigationController(rootViewController: profifleVc))
        
        setupChildViewController(HMProfileTableViewController(), imageName: "tabbar_profile", title: "我")
    }
    
    
    private func  setupChildViewController(controller:UIViewController, imageName:String, title:String)
    {
        //设置标题
        controller.title = title
        
        //设置图片
        controller.tabBarItem.image = UIImage(named:imageName)
        
        //包装一个导航控制器,并添加到tabbarVc里面
        addChildViewController(UINavigationController(rootViewController: controller))
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
