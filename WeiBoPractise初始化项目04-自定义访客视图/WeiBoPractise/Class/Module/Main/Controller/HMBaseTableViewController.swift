//
//  HMBaseTableViewController.swift
//  WeiBoPractise
//
//  Created by 邹花平 on 16/4/10.
//  Copyright © 2016年 邹花平. All rights reserved.
//

import UIKit

/**
   在这里建立一个UITableViewController父类,因为后面的HMHomeTableViewController等控制器以前都是UITableViewController,但是为了代码更加简洁,所以定义一个UITableViewController父类让它们继承,这样就让它还是UITableViewController
*/


class HMBaseTableViewController: UITableViewController//创建了HMBaseTableViewController记得让其他的home ,message等继承
{
    
    //用户是否登录
    var userLogin = false
    
    // 是控制器加载view的时候会调用,如果直接调用super.loadView(),后续该怎么加载就会怎么加载sb/xib,在loadView里面给view设置值后,那么就不会再去sb/xib
    
    //这里重写了这个loadView方法,然后UITableView加载出来了就不是带有格子的view了,而是根据下面的代码进行处理
    override func loadView()
    {
        //没有登录就调用自定义的一个方法setupVistorView
        userLogin ? super.loadView() : setupVistorView()//三目运算符
    }
    
    
    //设置访客视图
    private func setupVistorView()
    {
        view = visitorView
    
    }
   
     //MARK: - 懒加载数据
    /// 给别人调用的时候尽量简单
    /// 高内聚低耦合
    private lazy var visitorView: HMVisitorView = HMVisitorView()
    //相当于oc里面的[[HMVisitorView alloc] init]


   

    
}
