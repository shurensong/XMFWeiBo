//
//  HMBaseTableViewController.swift
//  WeiBoPractise
//
//  Created by 邹花平 on 16/4/10.
//  Copyright © 2016年 邹花平. All rights reserved.
//

import UIKit

/**
   在这里建立一个UITableViewController父类,因为后面的HMHomeTableViewController等控制器以前都是UITableViewController,但是为了代码更加简洁,所以定义一个UITableViewController父类让它们继承,这样就让它还是UITableViewController
*/


class HMBaseTableViewController: UITableViewController,HMVisitorViewDelegate
    //如果没有实现代理的方法,直接遵守协议立刻会报错,然后实现代理的方法之后就不会报错了
{
    
    //用户是否登录
    var userLogin = false
    
    // 是控制器加载view的时候会调用,如果直接调用super.loadView(),后续该怎么加载就会怎么加载sb/xib,在loadView里面给view设置值后,那么就不会再去sb/xib
    
    //这里重写了这个loadView方法,然后UITableView加载出来了就不是带有格子的view了,而是根据下面的代码进行处理
    override func loadView()
    {
        //没有登录就调用自定义的一个方法setupVistorView
        userLogin ? super.loadView() : setupVistorView()//三目运算符
    }
    
     //MARK: - 界面中的注册和登录按钮点击事件
    @objc private func didClickRegister()
    {
        print("didClickRegister")
    }
    
    @objc private func didClickLogin()
    {
        print("didClickLogin")

    }
    
    
     //MARK: - 实现代理的方法
    /// 6.需要实现协议里面的方法,如果不实现非optional的方法还是会报错

    func visitorViewDidClickRegisterButton(visitorView visitorView: UIView)
    {
        print("visitorViewDidClickRegisterButton")
    }
    
    func visitorViewDidClickLoginButton(visitorView visitorView: UIView)
    {
        print("visitorViewDidClickLoginButton")
    }
    
    //设置访客视图
    private func setupVistorView()
    {
        view = visitorView//就是我下面的HMVisitorView
        
        //5.设置访客视图的代理 ，当设置代理后，没有实现这个协议就会报错
        visitorView.delegate = self
        
        //设置访客视图内容 , 4个子控制器都会走这里, self具体是什么类型的看创建的子类: 多态 , 就是谁调用了这个方法就是谁 , 再则就是
        if self is HMHomeTableViewController
        {
            visitorView.startRotation()
        }
        else if self is HMMessageTableViewController
        {
            visitorView.setVisitorViewInfo("visitordiscover_image_message", message: "登录后，别人评论你的微博，发给你的消息，都会在这里收到通知")
        
        }
        
        else if self is HMDiscoverTableViewController
        {
            visitorView.setVisitorViewInfo("visitordiscover_image_message", message: "登录后，最新、最热微博尽在掌握，不再会与实事潮流擦肩而过")
        
        }
        else if self is HMProfileTableViewController
        {
            visitorView.setVisitorViewInfo("visitordiscover_image_profile", message: "登录后，你的微博、相册、个人资料会显示在这里，展示给别人")
        
        }
        
        
        
        //设置左右两个导航栏按钮, 注册和登录
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "注册", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("didClickRegister"))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "登录", style: UIBarButtonItemStyle.Plain, target: self, action: Selector("didClickLogin"))
        
    
    }
   
     //MARK: - 懒加载数据
    /// 给别人调用的时候尽量简单
    /// 高内聚低耦合
    private lazy var visitorView: HMVisitorView = HMVisitorView()
    //相当于oc里面的[[HMVisitorView alloc] init]


   

    
}
