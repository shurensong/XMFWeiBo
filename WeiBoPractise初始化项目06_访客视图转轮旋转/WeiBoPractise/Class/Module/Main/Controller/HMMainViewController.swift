//
//  HMMainViewController.swift
//  WeiBoPractise
//
//  Created by 邹花平 on 16/4/10.
//  Copyright © 2016年 邹花平. All rights reserved.
//

import UIKit

class HMMainViewController: UITabBarController {

    override func viewDidLoad()
    {
        super.viewDidLoad()
        let mainTabBar = HMMainTabBar()
        //3.定义闭包并设置
        mainTabBar.composeClosure =
            {
                () -> Void in
                print("加号按钮被点击了")
            }
        self.setValue(mainTabBar, forKey: "tabBar")

        setupChildViewControllers()
    }
    
    //往控制器添加子控制器代码的抽取
    private func setupChildViewControllers()
    {
        //首页
        
        setupChildViewController(HMHomeTableViewController(), imageName: "tabbar_home", title: "首页")
        
        //消息
        
        setupChildViewController(HMMessageTableViewController(), imageName: "tabbar_message_center", title: "消息")
        
        //发现
        
        setupChildViewController(HMDiscoverTableViewController(), imageName: "tabbar_discover", title: "发现")
        
        //我
        
        
        setupChildViewController(HMProfileTableViewController(), imageName: "tabbar_profile", title: "我")

    }
    
    private func  setupChildViewController(controller:UIViewController, imageName:String, title:String)
    {
        //设置标题
        controller.title = title
        
        //设置tabbar的普通图片
        controller.tabBarItem.image = UIImage(named:imageName)
        
        //设置tabbar的普通图片
        controller.tabBarItem.selectedImage = UIImage(named:imageName + "_selected")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        
        //设置tabbar的tintColor
        controller.tabBarItem.setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.orangeColor()], forState: UIControlState.Selected)
        
        // 设置tabbar的tintColor还有这个方法,但是当图片和文字颜色不一样时就用不了了,注意:这个方法是放在viewDidLoad方法里的
        //        tabBar.tintColor = UIColor.orangeColor()
        
        
        //包装一个导航控制器,并添加到tabbarVc里面
        addChildViewController(UINavigationController(rootViewController: controller))
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
