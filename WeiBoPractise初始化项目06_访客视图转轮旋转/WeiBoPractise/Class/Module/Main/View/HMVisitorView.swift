//
//  HMVisitorView.swift
//  WeiBoPractise
//
//  Created by 邹花平 on 16/4/10.
//  Copyright © 2016年 邹花平. All rights reserved.
//

import UIKit

//1.定义代理协议
protocol  HMVisitorViewDelegate : NSObjectProtocol
{
    //2.定义代理方法
    //把 visitorView传回去 , 而 swift 中默认第一个参数是不会生成外部参数名的 , 所以再来一个相同的参数就有了
    func visitorViewDidClickRegisterButton(visitorView visitorView: UIView)
    func visitorViewDidClickLoginButton(visitorView visitorView: UIView)
    
    
}




class HMVisitorView: UIView
{

    //3.定义代理属性
    weak var delegate : HMVisitorViewDelegate?
   
    
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        //准备UI
        prepareUI()
        backgroundColor = UIColor(white: 237 / 255.0, alpha: 1)
    }

     //MARK: - 公开方法
    
    //转轮动画
    func startRotation()
    {
        //不适合使用 UI 动画,因为这个时候要么180的时候就一直转,要么360的时候就不转
        
        //改用核心动画
        let anim = CABasicAnimation(keyPath: "transform.rotation")
        anim.toValue = M_PI * 2 // 旋转的角度
        anim.repeatCount = MAXFLOAT //重复旋转的次数
        anim.duration = 20 //旋转的时间
        
        //动画完成不要移除动画 , 如果没有下面的代码的话, 那么当界面切换到其他的界面然后回来的话就会取消掉动画,这样就没有图片一直在旋转的效果了
        anim.removedOnCompletion = false
        
        //给转轮图片添加动画效果
        iconView.layer.addAnimation(anim, forKey: nil)
        
    }
    
    //设置访客视图内容
    func setVisitorViewInfo(imageName : String , message : String)
    {
        //设置转轮的图片
        iconView.image = UIImage(named: imageName)
        
        //设置消息内容
        messageLabel.text = message
        
        //隐藏小房子
        homeView.hidden = true
        
        //隐藏遮盖
        coverView.hidden = true
        
    }
    
    

    private func prepareUI()
    {
        //1.添加控件
        addSubview(iconView)
       
        addSubview(coverView)
         addSubview(homeView)
        addSubview(messageLabel)
        addSubview(registerButton)
        addSubview(loginButton)
        
        //2.禁用系统自动布局
        iconView.translatesAutoresizingMaskIntoConstraints = false
        homeView.translatesAutoresizingMaskIntoConstraints = false
        coverView.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        registerButton.translatesAutoresizingMaskIntoConstraints = false
        loginButton.translatesAutoresizingMaskIntoConstraints = false
        
        //3.添加约束 将约束添加到父控件
            //1.转轮
        /**
        *  参数介绍
        / item: 要约束的view
        / attribute: 要约束的view的什么属性
        / relatedBy: 参照关系
        / toItem: 要参照的view
        / attribute: 参照的view的什么属性
        */
        self.addConstraint(NSLayoutConstraint(item: iconView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem:self, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: iconView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: -40))
        
        //2.遮盖
        //注意:不能这样的方式添加约束
//        self.coverView.addConstraint(NSLayoutConstraint(item: coverView, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Leading, multiplier: 1, constant: 0))
        
        self.addConstraint(NSLayoutConstraint(item: coverView, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Leading, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: coverView, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: coverView, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0))
        self
        .addConstraint(NSLayoutConstraint(item: coverView, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: registerButton, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0))
        
        
        //3.房子
        self.addConstraint(NSLayoutConstraint(item: homeView, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: iconView, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: homeView, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: iconView, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0))
        
        /// 4.消息label
        addConstraint(NSLayoutConstraint(item: messageLabel, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: iconView, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: messageLabel, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: iconView, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 16))
        
        /// 当参照的view为nil时,attribute必须设置为NSLayoutAttribute.NotAnAttribute
        addConstraint(NSLayoutConstraint(item: messageLabel, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 250))
        
        /// 注册按钮
        /// 阿拉伯语言
        addConstraint(NSLayoutConstraint(item: registerButton, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: messageLabel, attribute: NSLayoutAttribute.Leading, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: registerButton, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: messageLabel, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 16))
        
        addConstraint(NSLayoutConstraint(item: registerButton, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 100))
        addConstraint(NSLayoutConstraint(item: registerButton, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 35))
        
        /// 5.登录按钮
        addConstraint(NSLayoutConstraint(item: loginButton, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: messageLabel, attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: loginButton, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: registerButton, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0))
        
        addConstraint(NSLayoutConstraint(item: loginButton, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: registerButton, attribute: NSLayoutAttribute.Width, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: loginButton, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: registerButton, attribute: NSLayoutAttribute.Height, multiplier: 1, constant: 0))
        
    }
     //MARK: - 界面里的按钮点击事件
        //注册按钮点击事件
   @objc private func didClickRegisterButton()
    {
        //4.调用代理的协议方法
        delegate?.visitorViewDidClickRegisterButton(visitorView: self)
    }
   @objc private func didClickLoginButton()
    {
        delegate?.visitorViewDidClickLoginButton(visitorView: self)
    }
    
     //MARK: - 懒加载
    //1.转轮
    private lazy var iconView : UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_smallicon"))
    
    //2.小房子
    private lazy var homeView : UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_house"))
    
    //3.遮盖
    private lazy var coverView : UIImageView = UIImageView(image:UIImage(named: "visitordiscover_feed_mask_smallicon"))
    
    
    
    //4.消息文字
    private lazy var messageLabel : UILabel =
    {
        let label = UILabel()
        
        //设置label的相关属性
        label.text = "关注一些人!看看有什么惊喜!"
        label.font = UIFont.systemFontOfSize(18)
        label.numberOfLines = 0
//        label.textAlignment = NSTextAlignment.Center//这句敲了好多遍都提示有错误,最后是通过复制NSTextAlignmentCenter,然后在Center前面加了个点才没有报错了
       
        return label
    }()
    
    
    //6.注册按钮
    private lazy var registerButton : UIButton =
    {
        let button = UIButton()
        
        button.setTitle("注册", forState: UIControlState.Normal)
        button.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState: UIControlState.Normal)
        button.setTitleColor(UIColor.orangeColor(), forState: UIControlState.Normal)
        //如果没有给按钮添加方法就会导致点击之后没有响应
        button.addTarget(self, action: Selector("didClickRegisterButton"), forControlEvents: UIControlEvents.TouchUpInside)
         
        return button
    }()
    //7.登录按钮
    
    private lazy var loginButton : UIButton =
    {
        let button = UIButton()
        
        button.setTitle("登录", forState: UIControlState.Normal)
        button.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        button.setBackgroundImage(UIImage(named: "common_button_white_disable"), forState: UIControlState.Normal)
        
        button.addTarget(self, action: Selector("didClickLoginButton"), forControlEvents: UIControlEvents.TouchUpInside)
        return button
    
    }()

}
